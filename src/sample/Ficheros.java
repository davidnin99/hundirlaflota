package sample;

import java.io.*;

public class Ficheros {

    public void MostrarHistorial() throws IOException {
        FileReader fr=new FileReader("src\\sample\\Fichero.dat");
        BufferedReader breader= new BufferedReader(fr);
        String linea;
        while ( breader.readLine() != null) {
            linea = breader.readLine();
            if(linea != null){
                System.out.println(linea);
            }else{
                System.out.println(" ");
            }
        }
    };
    public void MostrarInstrucciones() throws IOException {
        FileReader fr=new FileReader("src\\sample\\Instrucciones.txt");
        BufferedReader breader= new BufferedReader(fr);
        String linea;
        while ( breader.readLine() != null) {
            linea = breader.readLine();
            if(linea != null){
                System.out.println(linea);
            }else{
                System.out.println(" ");
            }
        }

    };


    public void MostrarRanking() throws IOException {
        FileReader fr=new FileReader("src\\sample\\Ranking.txt");
        BufferedReader breader= new BufferedReader(fr);
        String linea  = breader.readLine();
        while ( breader.readLine() != null) {
            linea = breader.readLine();
            if(linea != null){
                System.out.println(linea);
            }else{
                System.out.println(" ");
            }
        }

    }

        public void ActualizarHistorial (int contador,String nombre) throws IOException {
            String ruta = "/home/mario/archivo.txt";
            File archivo = new File(ruta);
            BufferedWriter bw;
            if(archivo.exists()) {
                bw = new BufferedWriter(new FileWriter(archivo));
                bw.write("El usuario con nombre: "+ nombre+ " se ha quedado con: "+ contador +" vidas");
            } else {
                bw = new BufferedWriter(new FileWriter(archivo));
                bw.write("Acabo de crear el fichero de texto.");
            }
            bw.close();
        }


        public void ActualizarRanking(int contador, String nombre) throws IOException {
            String ruta = "/home/mario/archivo.txt";
            File archivo = new File(ruta);
            BufferedWriter bw;
            if(archivo.exists()) {
                bw = new BufferedWriter(new FileWriter(archivo));
                bw.write("El usuario con nombre: "+ nombre+ " se ha quedado con: "+ contador +" vidas");
            } else {
                bw = new BufferedWriter(new FileWriter(archivo));
                bw.write("Acabo de crear el fichero de texto.");
            }
            bw.close();
        };
}

