package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

//Main Ivan
//Todas las clases Juntas.
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        String nombreFichero = "prueba.txt";
        Scanner sc = new Scanner(System.in);
        Ficheros f1 = new Ficheros();
        int opcion;
        boolean salir = false;
        do {
            System.out.println("Bienvenido a Hunde la Flota");
            System.out.println("Que quieres hacer");
            System.out.println("1-Jugar");
            System.out.println("2- Ver instrucciones");
            System.out.println("3- Ver historial");
            System.out.println("4- Ver ranking");
            System.out.println("5- Salir");
            opcion = sc.nextInt();


            switch (opcion) {
                case 1:
                    Scanner sc2 = new Scanner(System.in);
                    Escritura e = new Escritura();
                    e.Crear();

                    boolean correcto = false;
                    boolean contador;
                    do {
                        contador = e.getTocado();
                        if (contador == true) {
                            do {
                                System.out.println("-----------------------------------");
                                System.out.println("¿A donde quieres disparar?");
                                System.out.println("Escribe la X.");
                                int x = parseInt(sc2.nextLine());
                                System.out.println("Escribe la Y.");
                                int y = parseInt(sc2.nextLine());
                                if (((x > 0) && (x < 11)) && ((y > 0) && (y < 11))) {
                                    correcto = true;
                                    e.Disparar(x, y);
                                } else {
                                    correcto = false;
                                }
                            } while (correcto == false);

                            contador = e.getTocado();
                            if (contador = true) {
                                salir = false;
                            } else {
                                salir = true;
                            }
                        } else {
                            salir = true;
                        }

                    } while (salir == false);
                    break;

                case 2:
                    f1.MostrarInstrucciones();
                    break;
                case 3:
                    f1.MostrarHistorial();
                    break;
                case 4:
                    f1.MostrarRanking();
                    break;
                case 5:
                    System.out.println("Se acabo lo que se daba locos");
                    salir=true;
                    break;
            }


        } while (salir != true);


    }

}

